# Getting Started
This is part of test assignment for Kiramitsu by Mihhail Novik

#Requirements
Implement search functionality using Java or Kotlin. 
This functionality allow customers to search in int system like a search engine using any keywords (food items name or restaurant name or location, etc.)

## Installing / Getting started

To compile:
- java 8
To Run:
- docker (https://docs.docker.com/install/)

# Technologies
- gradle build tool
- Spring boot
- Spring data solr (https://spring.io/projects/spring-data-solr)
- Project Lombok (https://projectlombok.org/) (if you have trouble with IDE make sure lombok plugin is installed)
- Testcontainers (https://www.testcontainers.org/)

# To see search examples
assignment.kiramitsu.fooddelivery.search.SearchServiceTest contains examples of different searches (by name, by description, fuzzy search, search score)
assignment.kiramitsu.fooddelivery.search.SearchControllerTest for web validation
To run tests you need docker to be installed.

# To run server locally
* run docker image with search engine 'docker run -p 8983:8983 -t solr solr-precreate search_item'
* start server ./gradlew bootRun
 
 After server is started systems can start to send requests to /search/items
 Examples with httpie command line tool (https://httpie.org/)
 
Too add new search items 
```
$ http -j POST http://localhost:8080/search/items id=food23 domain=food name="Fried egg" description="Fried egg with tomatoes" uri=http://facebook.com/tomatoes

HTTP/1.1 200 
Connection: keep-alive
Content-Type: application/json
Date: Sun, 01 Mar 2020 21:54:08 GMT
Keep-Alive: timeout=60
Transfer-Encoding: chunked
```

to search for items
```
$ http GET http://localhost:8080/search/items\?term\=egg                                                                                                       

HTTP/1.1 200 
Connection: keep-alive
Content-Type: application/json
Date: Sun, 01 Mar 2020 21:55:07 GMT
Keep-Alive: timeout=60
Transfer-Encoding: chunked

[
    {
        "description": "Fried egg with tomatoes",
        "name": "Fried egg",
        "uri": "http://facebook.com/tomatoes"
    }
]


```