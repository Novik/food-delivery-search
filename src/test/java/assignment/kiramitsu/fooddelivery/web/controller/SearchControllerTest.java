package assignment.kiramitsu.fooddelivery.web.controller;


import assignment.kiramitsu.fooddelivery.search.SearchService;
import assignment.kiramitsu.fooddelivery.search.solr.document.SearchItemDocument;
import assignment.kiramitsu.fooddelivery.web.model.SearchResponse;
import assignment.kiramitsu.fooddelivery.web.model.SearchableItem;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class SearchControllerTest {

    private final ObjectMapper mapper = new ObjectMapper();
    private final ObjectWriter writer = mapper.writer();

    @MockBean
    private SearchService searchService;
    @Autowired
    private MockMvc mockMvc;

    private Iterable<SearchResponse> mockResponse = Lists.list(new SearchResponse("uri", "description", "name"));
    private SearchItemDocument emptyDoc = SearchItemDocument.from(new SearchableItem("fakeID","shops", "name", "description", "uri"));

    @Test
    public void shouldReturnEmptyResultForShortTerm() throws Exception {
        mockMvc.perform(get("/search/items?term=aa")).andExpect(status().isOk())
                .andExpect(content().string(writer.writeValueAsString(Collections.emptyList())));
    }

    @Test
    public void shouldReturnValidResponse() throws Exception {
        when(searchService.search("food")).thenReturn(Lists.list(emptyDoc));
        mockMvc.perform(get("/search/items?term=food")).andExpect(status().isOk())
                .andExpect(content().json(writer.writeValueAsString(mockResponse)));
    }

    @Test
    public void shouldNotAcceptDocumentsWithoutId() throws Exception {
        when(searchService.index(any())).thenReturn(emptyDoc);
        SearchableItem invalidInput = new SearchableItem("", "", "", "", "");
        mockMvc.perform(post("/search/items")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(invalidInput))).andExpect(status().isBadRequest());
    }

    @Test
    public void shouldAccepDocumentToIndex() throws Exception {
        SearchableItem fullItem = new SearchableItem("myid-23", "items", "Pizza", "pizaa hut offer u a pizza", "http://mail.ru");
        SearchItemDocument searchItemDocument = SearchItemDocument.from(fullItem);
        when(searchService.index(any())).thenReturn(searchItemDocument);
        mockMvc.perform(post("/search/items")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(fullItem)))
                .andExpect(status().isOk())
                .andExpect(content().json(writer.writeValueAsString(searchItemDocument)));
    }
}
