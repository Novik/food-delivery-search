package assignment.kiramitsu.fooddelivery.search;

import assignment.kiramitsu.fooddelivery.search.solr.document.SearchItemDocument;
import assignment.kiramitsu.fooddelivery.web.model.SearchDomain;
import assignment.kiramitsu.fooddelivery.web.model.SearchableItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.FixedHostPortGenericContainer;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@Testcontainers
@SpringBootTest
public class SearchServiceTest {

    public static final SearchableItem SESAME_NOODLE_BAR = new SearchableItem("3", SearchDomain.RESTAURANT.name(), "Sesame Noodle Bar",
            "Vegetarian Friendly, Vegan Options, India food", "http://khmerburger.com.kh");
    public static final SearchableItem FLAVOURS_OF_INDIA = new SearchableItem("1", SearchDomain.RESTAURANT.name(), "Flavours of India",
            "Indian Food. Vegetarian Friendly, Vegan Options, Halal, Gluten Free Options.158, Street No 63, Phnom Penh 23, Cambodia", "https://www.facebook.com/Flavors-of-India-404420972998824/");
    public static final SearchableItem BURGERSHACK = new SearchableItem("2", SearchDomain.RESTAURANT.name(), "Burgershack",
            "Juicy burgers amazingly cooked with super friendly and awseome service", "http://khmerburger.com.kh");


    public static final SearchableItem TOMATO_SOUP = new SearchableItem("1", SearchDomain.FOOD.name(), "Tomato Soup", "soup made with tomatoes as the primary ingredient", "uri");
    public static final SearchableItem AMOK = new SearchableItem("2", SearchDomain.FOOD.name(), "Fish amok", " dish, made from fish, coconut milk and curry paste", "http://amok.com");
    public static final SearchableItem SAMLOR_MACHU = new SearchableItem("3", SearchDomain.FOOD.name(), "Samlor machu trey", "Soup include fish, garlic, lemongrass, celery, tamarind juice, bean sprouts",
            "http://amok.com");
    public static final SearchableItem NOM_BANH_CHOK = new SearchableItem("3", SearchDomain.FOOD.name(), "Nom banh chok (Khmer noodle)", "Rice noodles topped with green fish gravy and lots of fresh vegetables",
            "http://amok.com");

    @Container
    public static GenericContainer solr =
            new FixedHostPortGenericContainer("solr")
                    .withFixedExposedPort(8983, 8983).withCommand("solr-precreate search_item").waitingFor((Wait.forHttp("/")));
    @Autowired
    private SearchService searchService;

    @BeforeEach
    public void init() {
        searchService.deleteAll();
        // restaraunts
        searchService.index(FLAVOURS_OF_INDIA);
        searchService.index(BURGERSHACK);
        searchService.index(SESAME_NOODLE_BAR);

        // food items
        searchService.index(TOMATO_SOUP);
        searchService.index(AMOK);
        searchService.index(SAMLOR_MACHU);
        searchService.index(NOM_BANH_CHOK);
    }


    @Test
    public void shouldSearchByName() {
        List<SearchItemDocument> seasamNoodle = searchService.search("Sesame");
        assertThat(seasamNoodle.size()).isEqualTo(1);
        assertThat(seasamNoodle.get(0)).isEqualToComparingFieldByField(SearchItemDocument.from(SESAME_NOODLE_BAR));
    }

    @Test
    public void shouldSearchByDescription() {
        // both sesame and indian have 'vegan' in they description
        List<SearchItemDocument> veganResult = searchService.search("vegan");
        assertThat(veganResult.size()).isEqualTo(2);
        assertThat(veganResult.get(0)).isEqualToComparingFieldByField(SearchItemDocument.from(SESAME_NOODLE_BAR));
        assertThat(veganResult.get(1)).isEqualToComparingFieldByField(SearchItemDocument.from(FLAVOURS_OF_INDIA));
    }

    @Test
    public void shouldReturnBothMatchByNameAndByDescription() {
        // flavors of india has "india" in they name so it will be priority No 1
        // seasame has description
        List<SearchItemDocument> indiaResult = searchService.search("India");
        assertThat(indiaResult.size()).isEqualTo(2);
        assertThat(indiaResult.get(0)).isEqualToComparingFieldByField(SearchItemDocument.from(FLAVOURS_OF_INDIA));
        assertThat(indiaResult.get(1)).isEqualToComparingFieldByField(SearchItemDocument.from(SESAME_NOODLE_BAR));
    }

    @Test
    public void shouldSearchForRestarauntsAndFood() {
        // here we should have match with both 'seasame noodle' restaraunt and nom banh chok food
        List<SearchItemDocument> noodleResult = searchService.search("Noodle");
        assertThat(noodleResult.size()).isEqualTo(2);
        assertThat(noodleResult.get(0)).isEqualToComparingFieldByField(SearchItemDocument.from(SESAME_NOODLE_BAR));
        assertThat(noodleResult.get(1)).isEqualToComparingFieldByField(SearchItemDocument.from(NOM_BANH_CHOK));
    }

    @Test
    public void shouldFuzzySearch() {
        // even description has 'tomato' should should be able to search by 'tomatoes'
        List<SearchItemDocument> noodleResult = searchService.search("tomato");
        assertThat(noodleResult.size()).isEqualTo(1);
        assertThat(noodleResult.get(0)).isEqualToComparingFieldByField(SearchItemDocument.from(TOMATO_SOUP));
    }
}
