package assignment.kiramitsu.fooddelivery.search;

import assignment.kiramitsu.fooddelivery.search.solr.document.SearchItemDocument;
import assignment.kiramitsu.fooddelivery.web.model.SearchableItem;

import java.util.List;

public interface SearchService {

    List<SearchItemDocument> search(String term);

    SearchItemDocument index(SearchableItem searchableItem);

    void deleteAll();

}
