package assignment.kiramitsu.fooddelivery.search.solr.repository;

import assignment.kiramitsu.fooddelivery.search.solr.document.SearchItemDocument;
import org.springframework.data.solr.repository.Boost;
import org.springframework.data.solr.repository.SolrCrudRepository;

import java.util.List;

public interface SolrShopItemRepository extends SolrCrudRepository<SearchItemDocument, String> {
    List<SearchItemDocument> findByNameOrDescription(@Boost(2) String name, String description);
}
