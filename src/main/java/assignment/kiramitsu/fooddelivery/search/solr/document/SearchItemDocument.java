package assignment.kiramitsu.fooddelivery.search.solr.document;

import assignment.kiramitsu.fooddelivery.web.model.SearchableItem;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.Score;
import org.springframework.data.solr.core.mapping.SolrDocument;

/**
 * allow customers to search in system like a search engine using any keywords (food items name or restaurant name or location, etc.)
 */
@SolrDocument(collection = SearchItemDocument.SEARCH_ITEM)
public class SearchItemDocument {
    public static final String SEARCH_ITEM = "search_item";
    public static final String ID_FIELD = "document.id";
    public static final String NAME_FIELD = "name";
    public static final String DESCRIPTION_FIELD = "description";
    public static final String URI_FIELD = "uri";

    public SearchItemDocument() {

    }

    public SearchItemDocument(String id, String name, String description, String uri) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.uri = uri;
    }

    @Id
    @Indexed(value = ID_FIELD, type = "string")
    private String id;

    @Indexed(value = NAME_FIELD, type = "string")
    private String name;

    @Indexed(value = DESCRIPTION_FIELD, type = "string")
    private String description;

    @Indexed(value = URI_FIELD, type = "string")
    private String uri;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public static SearchItemDocument from(SearchableItem searchableItem) {
        SearchItemDocument shopItemDocument = new SearchItemDocument();
        shopItemDocument.setDescription(searchableItem.getDescription());
        shopItemDocument.setId(searchableItem.getGlobalId());
        shopItemDocument.setName(searchableItem.getName());
        shopItemDocument.setDescription(searchableItem.getDescription());
        shopItemDocument.setUri(searchableItem.getUri());
        return shopItemDocument;
    }
}
