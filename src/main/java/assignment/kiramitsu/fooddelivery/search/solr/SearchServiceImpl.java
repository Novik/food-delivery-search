package assignment.kiramitsu.fooddelivery.search.solr;

import assignment.kiramitsu.fooddelivery.search.SearchService;
import assignment.kiramitsu.fooddelivery.search.solr.document.SearchItemDocument;
import assignment.kiramitsu.fooddelivery.search.solr.repository.SolrShopItemRepository;
import assignment.kiramitsu.fooddelivery.web.model.SearchableItem;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SearchServiceImpl implements SearchService {

    private final SolrShopItemRepository shopItemRepository;
    private final String nameFuzziness = "~1";
    private final String descriptionFuzziness = "~3";

    @Override
    public List<SearchItemDocument> search(String term) {
        return shopItemRepository.findByNameOrDescription(term+nameFuzziness, term+descriptionFuzziness);
    }

    @Override
    @SneakyThrows
    public SearchItemDocument index(SearchableItem searchableItem) {
        val shopItemDocument = SearchItemDocument.from(searchableItem);
        return shopItemRepository.save(shopItemDocument);
    }

    @Override
    public void deleteAll() {
        shopItemRepository.deleteAll();
    }

    @PostConstruct
    public void init() {
        shopItemRepository.save(new SearchItemDocument("testID", "testName", "testDescription", "testUri"));
    }
}
