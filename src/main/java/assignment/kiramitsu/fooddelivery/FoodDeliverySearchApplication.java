package assignment.kiramitsu.fooddelivery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodDeliverySearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodDeliverySearchApplication.class, args);
	}

}
