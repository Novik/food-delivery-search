package assignment.kiramitsu.fooddelivery.web.model;

import assignment.kiramitsu.fooddelivery.search.solr.document.SearchItemDocument;
import lombok.Value;

@Value
public class SearchResponse {
    private String uri;
    private String description;
    private String name;

    public static SearchResponse fromDocument(SearchItemDocument itemDocument) {
        return new SearchResponse(itemDocument.getUri(), itemDocument.getDescription(), itemDocument.getName());
    }
}
