package assignment.kiramitsu.fooddelivery.web.model;

public enum SearchDomain {
    FOOD,
    RESTAURANT,
}
