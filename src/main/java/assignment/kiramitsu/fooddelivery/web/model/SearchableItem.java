package assignment.kiramitsu.fooddelivery.web.model;

import javax.validation.constraints.NotEmpty;

public class SearchableItem {
    public SearchableItem(@NotEmpty String id, @NotEmpty String domain, String name, String description, String uri) {
        this.id = id;
        this.domain = domain;
        this.name = name;
        this.description = description;
        this.uri = uri;
    }

    private @NotEmpty String id;
    private @NotEmpty String domain;
    private String name;
    private String description;
    private String uri;

    public String getGlobalId() {
        return domain+id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
