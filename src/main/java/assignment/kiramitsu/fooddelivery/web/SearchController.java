package assignment.kiramitsu.fooddelivery.web;

import assignment.kiramitsu.fooddelivery.search.SearchService;
import assignment.kiramitsu.fooddelivery.search.solr.document.SearchItemDocument;
import assignment.kiramitsu.fooddelivery.web.model.SearchResponse;
import assignment.kiramitsu.fooddelivery.web.model.SearchableItem;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SearchController {

    private final SearchService searchService;

    @GetMapping("/search/items")
    public Iterable<SearchResponse> fullTextSearch(@RequestParam String term) {
        if (term.length() < 3) {
            return Collections.emptyList();
        }
        return searchService.search(term).stream().map(SearchResponse::fromDocument).collect(Collectors.toList());
    }

    @PostMapping("/search/items")
    public SearchItemDocument indexItem(@RequestBody @Valid SearchableItem item) {
        return searchService.index(item);
    }

}
